using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
using Wilberforce;

public class GameManager : MonoBehaviour
{
    public PlayerStats player;
    public TMP_Text hp;
    public GameObject GameOverPanel;
    public TMP_Text score;
    public TMP_Text finalscore;
    private float puntuationxd = 0f;
    public TMP_Text highscore;
    public GameObject newhigh;
    public Colorblind colorblind;
    private float contador = 0f;
    private bool normalColor;
    
    public void ColorChange()
    {
        colorblind.Type = Random.Range(1,4);
    }

    public void GameOver()
    {
        hp.gameObject.SetActive(false);
        finalscore.text = score.text;
        SetHighScore();
        Time.timeScale = 0;
        GameOverPanel.SetActive(true);
    }

    public void RestartGame()
    {
        SceneManager.LoadScene("Game");
    }

    public void MainMenu()
    {
        SceneManager.LoadScene("MenuPrincipal");
    }

    void Start()
    {
        hp.text = player.hp + "hp";
        Time.timeScale = 1;
        score.text = "0";

    }

    // Update is called once per frame
    void Update()
    {
        hp.text = player.hp + "hp";
        contador += Time.deltaTime;
        puntuationxd += 20 * Time.deltaTime;
        score.text = Mathf.Round(puntuationxd) + "";
        NormalColor();
        if (!normalColor)
        {
            RandomColor();
        }
        
    }

    public void SetHighScore()
    {
        if(puntuationxd > PlayerPrefs.GetFloat("HighScore"))
        {
            PlayerPrefs.SetFloat("HighScore", Mathf.Round(puntuationxd));
            
            highscore.text = PlayerPrefs.GetFloat("HighScore") + "";
            newhigh.SetActive(true);
        }
        else
        {
            highscore.text = PlayerPrefs.GetFloat("HighScore") + "";
        }
    }

    public void NormalColor()
    {
        if(player.charge >= 100f)
        {
            player.charge = 0;
            contador = 0;
            normalColor = true;
            colorblind.Type = 0;
            normalColor = false;
        }
    }

    public void RandomColor()
    {
        if (contador >= 15f)
        {
            contador = 0;
            ColorChange();
        }
    }

}
