using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PlayerStats : MonoBehaviour
{

    public GameManager gm;

    public float hp = 200f;

    public float dmg = 20f;

    public float charge = 0f;

    public Slider lifeBar;

    public Slider chargeBar;



    private void Update()
    {
        if (hp <= 0f)
        {
            gm.GameOver();
            gameObject.SetActive(false);
        }

        lifeBar.value = hp/200f;

        chargeBar.value = charge / 100f;

    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "Enemy")
        {
            hp -= 20f;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Void"))
        {
            hp -= 1000f;
        }

        if (collision.CompareTag("EnemyProjectile"))
        {
            hp -= 10f;
            Destroy(collision.gameObject);
        }

        if (collision.transform.name.Contains("poison"))
        {
            hp -= 10;
            Destroy(collision.gameObject);
        }
        if (collision.transform.name.Contains("Orb"))
        {
            charge += 20f;
            Destroy(collision.gameObject);
        }
        if (collision.transform.name.Contains("Hp"))
        {
            if ( hp < 200f)
            {
                hp += 30;
            }
            if ( hp > 200f)
            {
                hp = 200f;
            }
            Destroy(collision.gameObject);
        }
    }


}
