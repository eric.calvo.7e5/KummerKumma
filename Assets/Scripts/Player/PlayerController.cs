using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public bool grounded;
    public float moveSpeed;
    public float jumpForce;
    private Rigidbody2D myRigidbody;
    public LayerMask whatIsGround;
    private Animator myAnimator;

    public Transform groundCheck;
    public float groundCheckRadius;
    public bool attack;
    public bool jumpAttack;


    // Start is called before the first frame update
    private void Start()
    {
        myRigidbody = GetComponent<Rigidbody2D>();

        myAnimator = GetComponent<Animator>();
    }

    // Update is called once per frame
    private void Update()
    {
        grounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, whatIsGround);


        myRigidbody.velocity = new Vector2(moveSpeed, myRigidbody.velocity.y);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (grounded)
            {
                myRigidbody.velocity = new Vector2(myRigidbody.velocity.x, jumpForce);
            }
        }

        if (Input.GetMouseButtonDown(0) && (!attack || !jumpAttack))
        {
            attack = true;
            jumpAttack = true;
            Invoke("AttackCD", 0.3f);
            Invoke("JumpAttackCD", 0.3f);
        }

        myAnimator.SetFloat("Speed", myRigidbody.velocity.x);
        myAnimator.SetBool("Grounded", grounded);
        myAnimator.SetBool("Attack", attack);
        myAnimator.SetBool("JumpAttack", jumpAttack);


    }

    public void AttackCD()
    {
        attack = false;
    }

    public void JumpAttackCD()
    {
        jumpAttack = false;
    }
    }
