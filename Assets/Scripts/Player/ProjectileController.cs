using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileController : MonoBehaviour
{

    public GameObject[] bullet;
    public float bulletSpeed;
    Vector3 myScreenPos;
    public bool shootPermission = true;
    public PlayerStats playerstats;

    void Start()
    {
        myScreenPos = Camera.main.WorldToScreenPoint(this.transform.position);
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (shootPermission == true)
            {
                Shoot();
            }
        }

    }

    public void ShootPermission()
    {
        shootPermission = true;
    }

    public void Shoot()
    {
        GameObject bulletShoot = (GameObject)Instantiate(bullet[Random.Range(0,1)], transform.position, Quaternion.identity);
        Vector3 direction = (Input.mousePosition - myScreenPos).normalized;
        bulletShoot.GetComponent<Rigidbody2D>().velocity = new Vector2(direction.x, direction.y) * bulletSpeed;
        bulletShoot.transform.right = (new Vector2(direction.x, direction.y) * bulletSpeed).normalized;

        shootPermission = false;
        Invoke("ShootPermission", 0.3f);
    }
}