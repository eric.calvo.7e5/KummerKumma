using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ScoreManager : MonoBehaviour
{
    public TMP_Text score;
    private float puntuationxd = 0f;

    // Start is called before the first frame update
    void Start()
    {
        score.text = "0";
    }

    // Update is called once per frame
    void Update()
    {
        puntuationxd += 20 * Time.deltaTime;
        score.text = Mathf.Round(puntuationxd) + "";
    }
}
