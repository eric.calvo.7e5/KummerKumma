using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CageDestroyer : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Projectile") || collision.CompareTag("Prop") || collision.CompareTag("EnemyProjectile"))
        {
            Destroy(collision.transform.gameObject);
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "Enemy")
        {
            Destroy(collision.transform.gameObject);
        }
    }
}
