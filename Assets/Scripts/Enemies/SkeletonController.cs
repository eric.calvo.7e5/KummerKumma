using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkeletonController : MonoBehaviour
{
    private bool _lookingRight;
    private Vector2 _vectorDireccion = new Vector2(-2f, -1f);
    private bool _canMove = true;
    public float moveSpeed;
    private Rigidbody2D myRigidbody;
    public LayerMask whatIsGround;
    private Animator myAnimator;
    public float hp = 50f;

    // Start is called before the first frame update
    private void Start()
    {
        myRigidbody = GetComponent<Rigidbody2D>();

        myAnimator = GetComponent<Animator>();
    }

    private void Update()
    {
        if (!myRigidbody) return;
        if (_canMove)
        {
            if (!_lookingRight)
            {
               
                _lookingRight = false;
                myRigidbody.velocity = new Vector2(-moveSpeed, myRigidbody.velocity.y);
                gameObject.GetComponent<SpriteRenderer>().flipX = false;
               
            }
            else
            {
                _lookingRight = true;

                myRigidbody.velocity = new Vector2(moveSpeed, myRigidbody.velocity.y);

                gameObject.GetComponent<SpriteRenderer>().flipX = true;
            }
        }
        if (_lookingRight)
        {
            _vectorDireccion = new Vector2(1, 0) + Vector2.down;
        }
        else
        {
            _vectorDireccion = new Vector2(-1, 0) + Vector2.down;
        }
        RaycastHit2D hitFront = Physics2D.Raycast(transform.position, _vectorDireccion, 1.5f, LayerMask.GetMask("Platforms"));
        if (hitFront.collider == null)
        {
            _lookingRight = !_lookingRight;
            _canMove = false;
            Invoke("MovementPermission", 0.5f);
        }
        if (hp <= 0f)
        {
            SkeletonDeath();
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "Player")
        {
            hp -= 50f;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Projectile"))
        {
            hp -= 20f;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawRay(transform.position, _vectorDireccion);
    }

    private void MovementPermission()
    {
        _canMove = true;
    }

    public void SkeletonDeath()
    {
        Destroy(GetComponent<Rigidbody2D>());
        gameObject.GetComponent<CapsuleCollider2D>().enabled = false;
        myAnimator.SetBool("Dead", true);
        Destroy(gameObject, 0.5f);
    }

}