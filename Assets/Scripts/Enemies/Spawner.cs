using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{

    public GameObject[] enemies;
    public int enemySelector;

    // Start is called before the first frame update
    private void Start()
    {
        
    }

    private void Update()
    {
        

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("ground"))
        {
            enemySelector = Random.Range(0, 2);
            Instantiate(enemies[enemySelector], new Vector2(collision.transform.position.x + Random.Range(-2,2), collision.transform.position.y + 2f), Quaternion.identity);
            
        }
        
    }

}
