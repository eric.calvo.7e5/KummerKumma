using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostController : MonoBehaviour
{
    public float hp = 30;
    private Animator myAnimator;
    public GameObject bullet;
    public Transform bulletPos;
    private float timer;
    private GameObject player;

    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        myAnimator = GetComponent<Animator>();
    }

    
    void Update()
    {
        float distance = Vector2.Distance(transform.position, player.transform.position);
        if(distance < 14f)
        {
            timer += Time.deltaTime;
            if (timer > 2)
            {
                myAnimator.SetTrigger("Attack");
                timer = 0;
                Invoke("Shoot", 0.4f);
            }

        }

        if (hp <= 0f)
        {
            GhostDeath();
        }
    }

    public void Shoot()
    {
        Instantiate(bullet, bulletPos.position, Quaternion.identity);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Projectile"))
        {
            hp -= 20f;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "Player")
        {
            hp -= 30f;
        }
    }
    public void GhostDeath()
    {
        Destroy(GetComponent<Rigidbody2D>());
        gameObject.GetComponent<CapsuleCollider2D>().enabled = false;
        myAnimator.SetTrigger("Ded");
        Destroy(gameObject, 0.6f);
    }
}
