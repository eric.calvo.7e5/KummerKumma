using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformGenerator : MonoBehaviour
{

    public GameObject platform;
    public Transform generationPoint;
    public float distanceBetween;

    public float distanceBetweenMin;
    public float distanceBetweenMax;

    public GameObject[] platforms;
    private int platformSelector;
    private float[] platformWidths;
    private int lastPlatformSelector = 1;

    // Start is called before the first frame update
    void Start()
    {
        //platformWidth = platform.GetComponent<BoxCollider2D>().size.x;
        platformWidths = new float[platforms.Length];

        for (int i = 0; i < platforms.Length; i++)
        {
            platformWidths[i] = platforms[i].GetComponent<BoxCollider2D>().size.x;
        }
    }

    // Update is called once per frame
    void Update()
    {
        

        if (transform.position.x < generationPoint.position.x)
        {

            distanceBetween = Random.Range(distanceBetweenMin, distanceBetweenMax);
            
            platformSelector = Random.Range(0,platforms.Length);

            transform.position = new Vector3(transform.position.x + platformWidths[platformSelector] / 2 + platformWidths[lastPlatformSelector] / 2 + distanceBetween, transform.position.y + Random.Range(-2, 2), transform.position.z); ;

            lastPlatformSelector = platformSelector;

            Instantiate(platforms[platformSelector], transform.position, Quaternion.identity);
        }


    }
}
