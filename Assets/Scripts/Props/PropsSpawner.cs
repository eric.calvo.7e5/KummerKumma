using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PropsSpawner : MonoBehaviour
{

    public GameObject[] props;
    public int propSelector;

    // Start is called before the first frame update
    private void Start()
    {

    }

    private void Update()
    {


    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("ground"))
        {
            propSelector = Random.Range(0, 3);
            Instantiate(props[propSelector], new Vector2(collision.transform.position.x + Random.Range(-1, 2), collision.transform.position.y + 2f), Quaternion.identity);
        }

    }

}